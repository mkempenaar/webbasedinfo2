package nl.bioinf.marcelk.apiconsumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by marcelk on 16/05/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiconsumerApplication.class)
@AutoConfigureMockMvc
public class GeneSymbolTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void GeneSymbolResource() throws Exception {
        // Call the methods to test
        translateSymbols();

        // ...
    }

    /**
     * Tests the `genesymbol` endpoint
     * @throws Exception
     */
    private void translateSymbols() throws Exception {
        // Expected JSON result
        String responseJson = "[{'type':'gene','id':'ENSG00000139618'},{'type':'gene','id':'LRG_293'}]";

        // Perform a GET request for the human BRCA2 gene
        this.mockMvc.perform(get("/apiconsumer/api/v1/genesymbol?species={species}&symbol={genesymbol}&",
                "9606", "BRCA2"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson));
    }
}
