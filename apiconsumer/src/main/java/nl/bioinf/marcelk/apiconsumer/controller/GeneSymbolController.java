package nl.bioinf.marcelk.apiconsumer.controller;

import nl.bioinf.marcelk.apiconsumer.dto.EnsemblIdDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by marcelk on 03/05/2017.
 */
@RestController
@RequestMapping(value = "/apiconsumer/api/v1")
public class GeneSymbolController {

    private RestTemplate template = new RestTemplate();

    /**
     * Translates a gene symbol given a species name to an Ensembl stable identifier
     *  Example: /apiconsumer/api/v1/genesymbol?species=human&symbol=BRCA1
     * @param species Species name, alias or taxonomy ID
     * @param symbol Gene symbol or common name
     * @return JSON data containing all Ensembl IDs for the given gene
     */
    @RequestMapping(value = "/genesymbol", method = RequestMethod.GET,
                    produces = "application/json")
    public List<EnsemblIdDTO> translateGeneSymbol(@RequestParam(value = "species") String species,
                                                  @RequestParam(value = "symbol") String symbol) {

        // Ensembl REST API URL.
        // Note: use application.properties for storing static URL part and content type
        String EnsemblServiceURL =
                String.format("https://rest.ensembl.org/xrefs/symbol/%s/%s?content-type=application/json",
                              species, symbol);

        // This object is just a type reference to tell the `template.exchange()` method that
        // we want the received data stored in a List of EnsemblIdDTO objects.
        ParameterizedTypeReference<List<EnsemblIdDTO>> listOfEnsemblIdDTOs =
                new ParameterizedTypeReference<List<EnsemblIdDTO>>() {};
        /*
            Retrieves the JSON data from Ensembl (given the EnsemblServiceURL) and maps this to one
            or more EnsemblIdDTO objects.
            Arguments to template.exchange are as follows:
                - REST URL for retrieving JSON input data.
                - HTTP method for retrieving the data (GET in this case).
                - An optional `requestEntity` (extension of `HttpEntity`) that represents a HTTP request or
                  response containing both headers and body.
                - The type reference we want the data to be stored in.
         */
        ResponseEntity<List<EnsemblIdDTO>> ensemblResponse = template.exchange(EnsemblServiceURL,
                HttpMethod.GET, null, listOfEnsemblIdDTOs);

        // 'copy' the actual list of objects for return, stored as body in the `ensemblResponse`
        List<EnsemblIdDTO> response = ensemblResponse.getBody();

        // Return List object, Spring will translate this to a JSON response
        return response;
    }
}
