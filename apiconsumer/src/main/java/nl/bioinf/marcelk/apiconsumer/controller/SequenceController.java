package nl.bioinf.marcelk.apiconsumer.controller;

import nl.bioinf.marcelk.apiconsumer.dto.SequenceDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


/**
 * Created by marcelk on 03/05/2017.
 */
@RestController
@RequestMapping(value = "/apiconsumer/api/v1")
public class SequenceController {

    private RestTemplate template = new RestTemplate();

    /**
     * Downloads the sequence given an Ensembl ID
     *  Example: /apiconsumer/api/v1/sequence?ensemblId=ENSMUSG00000040782
     * @param id an Ensembl stable ID for either nucleotide or protein sequence
     * @return JSON data containing a sequence and some metadata
     */
    @RequestMapping(value = "/sequence", method = RequestMethod.GET,
                    produces = "application/json")
    public SequenceDTO retrieveSequence(@RequestParam(value = "id") String id) {

        // Ensembl REST API URL.
        // Note: use application.properties for storing static URL part and content type
        String EnsemblServiceURL =
                String.format("https://rest.ensembl.org/sequence/id/%s?content-type=application/json", id);

        /*
        The response object is a direct mapping of the Ensembl response data using the `template.getForObject`
        call. This `RestTemplate` method calls the EnsemblServiceURL and maps it to a new SequenceDTO object.
         */
        SequenceDTO response = template.getForObject(EnsemblServiceURL, SequenceDTO.class);
        // Add some sequence statistic to the object manually
        response.calculateLength();

        // Return SequenceDTO object, Spring will translate this to a JSON response
        return response;
    }
}
