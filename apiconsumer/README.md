# REST API Consumer Demo Project

This small Java Spring project shows how to *consume* (use) a REST API, specifically focusing on mapping the JSON
response to Java object(s) for further use. This project 'prints' the object contents as a JSON response.

# REST API

Default project REST API location: 
[`http://http://localhost:8080/apiconsumer/api/v1/`](http://localhost:8080/apiconsumer/api/v1/)

## `/genesymbol` ([Ensembl API](https://rest.ensembl.org/documentation/info/xref_external))

Translating a gene symbol (common name) to Ensembl stable ID for use with other Ensembl API features.
Response is a JSON *List* of result entries.

### Parameters

#### Required

| Name  | Type  | Description  | Default  | Example Values  |
|---|---|---|---|---|
| species  | *String*  | A species name, alias or taxid  | -  | homo_sapiens/human  |
| symbol | *String* | A gene symbol | - | BRCA2 |

### Example Requests

`/genesymbol?species=10090&symbol=COP1`

#### Result

```json
[
  {
    "featureType": "gene",
    "ensemblId": "ENSMUSG00000040782",
    "species": null,
    "symbol": null
  }
]
```

`/genesymbol?species=human&symbol=COP1`

#### Result

````json
[
  {
    "featureType": "gene",
    "ensemblId": "ENSG00000204397",
    "species": null,
    "symbol": null
  },
  {
    "featureType": "gene",
    "ensemblId": "ENSG00000143207",
    "species": null,
    "symbol": null
  }
]
````

## `/sequence` ([Ensembl API](https://rest.ensembl.org/documentation/info/sequence_id))

Retrieving the full sequence given an Ensembl stable gene or protein ID.

### Parameters

#### Required

| Name  | Type  | Description  | Default  | Example Values  |
|---|---|---|---|---|
| ensemblId  | *String*  | An Ensembl stable ID  | -  |  ENSMUSG00000040782, ENSP00000288602 |

### Example Requests

`/sequence?ensemblId=ENSMUSG00000040782`

#### Result

**Note**: `seq` is truncated
```json
{
  "description": "chromosome:GRCm38:1:159232320:159347640:1",
  "ensemblId": "ENSMUSG00000040782",
  "molecule": "dna",
  "length": 115321,
  "seq": "CCGGCCCGTCTCCCGGCGCGTCCTCAGGATGCTCGGGAGCTGAGCGCCGGGGGCGGCGGAG..."
}
```

## Notes

* Basic Spring [Consuming Rest](https://spring.io/guides/gs/consuming-rest/) data guide.
* See the [Jackson Annotations](http://www.baeldung.com/jackson-annotations) example/ tutorial for more examples of
annotating a DTO class for mapping JSON data.
* Some [brief explanation](http://yetanotherdevblog.com/introduction_to_spring_resttemplate_series_1)
 regarding the Spring `RestTemplate` object