package nl.bioinf.marcelk.ebimuscle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EbiMuscleApplication {

	public static void main(String[] args) {
		SpringApplication.run(EbiMuscleApplication.class, args);
	}
}
