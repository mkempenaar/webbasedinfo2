# REST Using a Webservice

This small Java Spring project shows how to POST data to a webservice, combining parameters with a file.

# REST API

Default project REST API location: 
[`http://http://localhost:8080/ebimuscle/api/v1/`](http://localhost:8080/ebimuscle/api/v1/)

## `/runmuscle` ([EBI MUSCLE API](http://www.ebi.ac.uk/Tools/webservices/services/msa/muscle_rest))

Performs a Multiple Sequence Alignment by submitting sequences to the online MUSCLE API at EBI.

**Note**: This is not a proper Restful API, more a demonstrator for the Spring `RestTemplate` object 
for GETting and POSTing data to other webservices. The response is either a message (i.e. `ERROR`, `FAILURE`)
or the alignment as received from MUSCLE. Some information during the run is printed to stdout.

### Parameters

#### Required

| Name  | Type  | Description  | Default  | Example Values  |
|---|---|---|---|---|
| file  | File  | A file containing sequence data using the format as defined with the 'format' parameter | fasta  | - |

#### Optional

| Name  | Type  | Description  | Default  | Example Values  |
|---|---|---|---|---|
| email  | String  | Email field for EBI, must be a valid address | \<hidden\>  | my.name@st.hanze.nl |
| title  | String  | Title of the submission | MUSCLE  | - |
| format  | String  | Input file format | fasta  | - |
| tree  | String  | MUSCLE tree parameter | none  | - |
| order  | String  | MUSCLE order parameter | aligned  | - |
| outfmt  | String  | Output format | aln-fasta  | - |

