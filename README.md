## Web-based Information Systems 2 - Course Repository

This repository contains projects created for and during the course.

* ###[Spring ApiConsumer](https://bitbucket.org/mkempenaar/webbasedinfo2/raw/master/apiconsumer/)

    **Spring project demonstrating how to consume a REST API**  
      
    This small project calls two different [Ensembl API](https://rest.ensembl.org) endpoints, currently offering a conversion between gene symbols and Ensembl IDs and a sequence download API given an Ensembl ID.


### Links

* ###[NCBI E-Utilities Documentation](https://www.ncbi.nlm.nih.gov/books/NBK25499/)

    ** i.e. search for "*Fetch the GenBank flat file *".